<?php

namespace App\Repository;

use App\Entity\IngredientsList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IngredientsList|null find($id, $lockMode = null, $lockVersion = null)
 * @method IngredientsList|null findOneBy(array $criteria, array $orderBy = null)
 * @method IngredientsList[]    findAll()
 * @method IngredientsList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientsListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IngredientsList::class);
    }

    // /**
    //  * @return IngredientsList[] Returns an array of IngredientsList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IngredientsList
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
