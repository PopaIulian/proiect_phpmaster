<?php

namespace App\Repository;

use App\Entity\MixVegetables;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MixVegetables|null find($id, $lockMode = null, $lockVersion = null)
 * @method MixVegetables|null findOneBy(array $criteria, array $orderBy = null)
 * @method MixVegetables[]    findAll()
 * @method MixVegetables[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MixVegetablesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MixVegetables::class);
    }

    // /**
    //  * @return MixVegetables[] Returns an array of MixVegetables objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MixVegetables
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
