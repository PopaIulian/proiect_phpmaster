<?php

namespace App\Repository;

use App\Entity\MeatType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MeatType|null find($id, $lockMode = null, $lockVersion = null)
 * @method MeatType|null findOneBy(array $criteria, array $orderBy = null)
 * @method MeatType[]    findAll()
 * @method MeatType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeatTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MeatType::class);
    }

    // /**
    //  * @return MeatType[] Returns an array of MeatType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MeatType
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
