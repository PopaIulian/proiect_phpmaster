<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $pizzaId;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $addressId;

    /**
     * @ORM\Column(type="integer")
     */
    private $nrOfPizza;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPizzaId(): ?int
    {
        return $this->pizzaId;
    }

    public function setPizzaId(int $pizzaId): self
    {
        $this->pizzaId = $pizzaId;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getAddressId(): ?int
    {
        return $this->addressId;
    }

    public function setAddressId(?int $addressId): self
    {
        $this->addressId = $addressId;

        return $this;
    }

    public function getNrOfPizza(): ?int
    {
        return $this->nrOfPizza;
    }

    public function setNrOfPizza(int $nrOfPizza): self
    {
        $this->nrOfPizza = $nrOfPizza;

        return $this;
    }
}
