<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MixVegetablesRepository")
 */
class MixVegetables
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $mixId;

    /**
     * @ORM\Column(type="integer")
     */
    private $vegetableId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMixId(): ?int
    {
        return $this->mixId;
    }

    public function setMixId(int $mixId): self
    {
        $this->mixId = $mixId;

        return $this;
    }

    public function getVegetableId(): ?int
    {
        return $this->vegetableId;
    }

    public function setVegetableId(int $vegetableId): self
    {
        $this->vegetableId = $vegetableId;

        return $this;
    }
}
