<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientsListRepository")
 */
class IngredientsList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $doughId;

    /**
     * @ORM\Column(type="integer")
     */
    private $meatId;

    /**
     * @ORM\Column(type="integer")
     */
    private $mixVegetablesId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoughId(): ?int
    {
        return $this->doughId;
    }

    public function setDoughId(int $doughId): self
    {
        $this->doughId = $doughId;

        return $this;
    }

    public function getMeatId(): ?int
    {
        return $this->meatId;
    }

    public function setMeatId(int $meatId): self
    {
        $this->meatId = $meatId;

        return $this;
    }

    public function getMixVegetablesId(): ?int
    {
        return $this->mixVegetablesId;
    }

    public function setMixVegetablesId(int $mixVegetablesId): self
    {
        $this->mixVegetablesId = $mixVegetablesId;

        return $this;
    }
}
