<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IngredientsListController extends AbstractController
{
    /**
     * @Route("/ingredients/list", name="ingredients_list")
     */
    public function index()
    {
        return $this->render('ingredients_list/index.html.twig', [
            'controller_name' => 'IngredientsListController',
        ]);
    }
}
