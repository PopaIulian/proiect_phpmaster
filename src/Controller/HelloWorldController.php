<?php

namespace App\Controller;

use App\Entity\HelloWorld;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HelloWorldController extends AbstractController
{

    public function createHello():Response
    {
        $entity= $this->getDoctrine()->getManager();

        $hello = new HelloWorld();
        $hello->setText("Hello world!");

        $entity->persistent($hello);

        $entity->flush();

        return new Response('Saved ne hw with id'.$hello->getId());
    }
    
    /**
     * @Route("/hello/world", name="hello_world")
     */
    public function index($id)
    {
        $hello= $this->GetDoctrine()
        ->getRepository(HelloWorld::class)
        ->find($id);

        if(!$hello)
        {
                throw $this->createNotFoundException(
                    "Not found".$id
                );
        }
        return new Response('<h1>'.$hello->getText());
    }
}
