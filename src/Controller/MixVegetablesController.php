<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MixVegetablesController extends AbstractController
{
    /**
     * @Route("/mix/vegetables", name="mix_vegetables")
     */
    public function index()
    {
        return $this->render('mix_vegetables/index.html.twig', [
            'controller_name' => 'MixVegetablesController',
        ]);
    }
}
