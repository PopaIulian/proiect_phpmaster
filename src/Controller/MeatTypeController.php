<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MeatTypeController extends AbstractController
{
    /**
     * @Route("/meat/type", name="meat_type")
     */
    public function index()
    {
        return $this->render('meat_type/index.html.twig', [
            'controller_name' => 'MeatTypeController',
        ]);
    }
}
