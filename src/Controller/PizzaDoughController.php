<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PizzaDoughController extends AbstractController
{
    /**
     * @Route("/pizza/dough", name="pizza_dough")
     */
    public function index()
    {
        return $this->render('pizza_dough/index.html.twig', [
            'controller_name' => 'PizzaDoughController',
        ]);
    }
}
