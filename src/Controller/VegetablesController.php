<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VegetablesController extends AbstractController
{
    /**
     * @Route("/vegetables", name="vegetables")
     */
    public function index()
    {
        return $this->render('vegetables/index.html.twig', [
            'controller_name' => 'VegetablesController',
        ]);
    }
}
